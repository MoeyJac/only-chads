import { server } from '../../../config'
import Banner from '../../../components/Banner'
import Meta from '../../../components/Meta'
import creatorStyles from '../../../styles/Creator.module.css'

const creator = ({creator}) => {
    return (
        <>
        <Meta title={creator.name + '\'s page'} description={creator.bio}/>
        <div className={`${creatorStyles.profile}`}>
          <Banner creator={creator}/>
        </div>
        </>
    )
}

export const getStaticProps = async (context) => {
    const res = await fetch(`${server}/api/creators/${context.params.id}`)
  
    const creator = await res.json()
  
    return {
      props: {
        creator,
      },
    }
  }

export const getStaticPaths = async () => {
    const res = await fetch(`${server}/api/creators`)
  
    const creators = await res.json()
  
    const ids = creators.map((creator) => creator.id)
    const paths = ids.map((id) => ({ params: { id: id.toString() } }))
  
    return {
      paths,
      fallback: false,
    }
  }

export default creator