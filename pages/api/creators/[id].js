import { creators } from '../../../creators'

export default function handler({ query: { id } }, res) {
  const filtered = creators.filter((creator) => creator.id === id)

  if (filtered.length > 0) {
    res.status(200).json(filtered[0])
  } else {
    res
      .status(404)
      .json({ message: `Creator with the id of ${id} is not found` })
  }
}