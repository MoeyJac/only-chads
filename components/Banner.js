import { urlObjectKeys } from 'next/dist/next-server/lib/utils'
import bannerStyles from '../styles/Banner.module.css'

const Banner = ({ creator }) => {
    return (
        <div>
            <div className={bannerStyles.bCompactHeader}>
                <div className={bannerStyles.bannertext}>
                    <h3>{creator.name}</h3>
                </div>
            </div>
            <div className={bannerStyles.profileHeader}>
                <img className={bannerStyles.img} src={creator.bannerurl} alt={creator.name} />
            </div>
        </div>
    )
}

export default Banner