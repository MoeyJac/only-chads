import Link from 'next/link'
import search from '../styles/Search.module.css'

const Search = () => {
    return (
        <div>
            <input className={search.input} placeholder='Search'/>
        </div>
    )
}

export default Search