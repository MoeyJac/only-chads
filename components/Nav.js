import Link from 'next/link'
import navStyles from '../styles/Nav.module.css'
import Search from './Search'

const Nav = () => {
    return (
        <nav className={navStyles.nav}>
            <div>
                <Link href='/'>OnlyChads</Link>
            </div>
            <div>
                <Search />
            </div>
            <div>
                <Link href='/login'>Login</Link>
            </div>
        </nav>
    )
}

export default Nav